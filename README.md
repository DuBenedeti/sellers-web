WEB Desafio Técnico Tray

Requisitos
- PHP 8.1
- Docker 

Configurar .env
- copiar .env.example
- php artisan key:generate para gerar a APP_KEY

Após fazer o clone, inicializar o docker com 
- docker-compose up -d

Para acessar no browser
- localhost:8001

A conexão com a API está sendo feita através do link que subi ela
Caso queira alterar, ele está nos arquivos SellerApiRepository.php e SaleApiRepository.php