<?php

namespace App\Repositories;

use App\Repositories\ApiRepository;

class SellerApiRepository extends ApiRepository
{
    public function __construct()
    {
        parent::__construct('https://api.ecompdigital.com.br/api/');
    }

    public function getSellers()
    {
        $url = $this->buildUrl(['seller']);
        return $this->performHttpGet($url);
    }
    public function postNewSeller($values)
    {
        $url = $this->buildUrl(['seller'], ['name' => $values['name'], 'email' => $values['email']]);
        return $this->performHttpPost($url);
    }
}
