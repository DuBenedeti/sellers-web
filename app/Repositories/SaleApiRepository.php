<?php

namespace App\Repositories;

use App\Repositories\ApiRepository;

class SaleApiRepository extends ApiRepository
{
    public function __construct()
    {
        parent::__construct('https://api.ecompdigital.com.br/api/');
    }

    public function getSaleByUser($sellers_id = null)
    {
        $url = $this->buildUrl(['sale'], ['sellers_id' => $sellers_id]);
        return $this->performHttpGet($url);
    }
    public function postNewSale($values)
    {
        $url = $this->buildUrl(['sale'], ['sellers_id' => $values['sellers_id'], 'sale_value' => $values['sale_value']]);
        return $this->performHttpPost($url);
    }
}
