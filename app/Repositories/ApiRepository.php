<?php

namespace App\Repositories;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;

class ApiRepository
{
  private $client;

  public function __construct(string $baseURL)
  {
    $this->baseURL = $baseURL;
  }
  
  protected function getClient(): Client
  {
    return $this->client;
  }
  
  
  protected function performHttpGet(string $url)
  {
      try {
        $response = Http::get($this->baseURL . $url);

      if ($response->getStatusCode() !== 200) {
        \Log::info('Finish request to ' . $url . ' with: ' . $response->getBody());
        throw new \Exception('Status Code: ' . $response->getStatusCode());
      }
    } catch (\Exception $e) {
      \Log::error('Error perform HTTP GET. ' . $e->getMessage());

      return null;
    }

    return (object) json_decode($response->getBody(), false);
  }

  protected function performHttpPost(string $url)
  {

    try {
      $response = Http::post($this->baseURL . $url);
      if ($response->getStatusCode() !== 201 && $response->getStatusCode() !== 200) {
        \Log::info('Finish request to ' . $url . ' with: ' . $response->getBody());
        throw new \Exception('Status Code: ' . $response->getStatusCode());
      }
    } catch (\Exception $e) {
      \Log::error('Error perform HTTP POST. ' . $e->getMessage());

      return null;
    }

    return json_decode($response->getBody(), false);
  }

  protected function buildURL(array $route, array $params = []): string
  {
    $route = implode("/", $route);

    if (!empty($params)) {
      $route .= '?' . http_build_query($params);
    }

    return urldecode($route);
  }
}
