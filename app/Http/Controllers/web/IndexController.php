<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Seller;
use App\Repositories\SaleApiRepository;
use App\Repositories\SellerApiRepository;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(){

        $sellersApi = new SellerApiRepository();
        $sellers = $sellersApi->getSellers();

        return view('front.vendedores', compact('sellers'));
    }
    
    public function vendas($sellers_id){

        $saleApi = new SaleApiRepository();
        $sales = $saleApi->getSaleByUser($sellers_id);
        return view('front.vendas', compact('sales'));
    }

    public function vendasCreate(){

        $sellersApi = new SellerApiRepository();
        $sellers = $sellersApi->getSellers();

        return view('front.vendas-create', compact('sellers'));
    }

    public function vendasStore (Request $request) {
        $saleApi = new SaleApiRepository();
        $sales = $saleApi->postNewSale($request->all());
        return redirect()->route('vendas.index', $request->sellers_id);
    }
    
    public function vendedorCreate(){

        return view('front.vendas-create');
    }

    public function vendedorStore (Request $request) {
        $sellerApi = new SellerApiRepository();
        $seller = $sellerApi->postNewSeller($request->all());
        return redirect()->route('index', $request->sellers_id);
    }
}
