<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;
    protected $table = 'sale';

    protected $fillable = [
        'sellers_id',
        'sale_value',
        'commission',
    ];
    protected $data = [
        'created_at',
        'updated_at'
    ];

    public function seller()
    {
        return $this->belongsTo('App\Models\Seller', 'sellers_id');
    }
}
