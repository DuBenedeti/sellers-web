<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'web\IndexController@index')->name('index');
Route::get('/vendas/{sellers_id}', 'web\IndexController@vendas')->name('vendas.index');
Route::get('/form/vendas/create', 'web\IndexController@vendasCreate')->name('vendas.create');
Route::post('/form/vendas/store', 'web\IndexController@vendasStore')->name('vendas.store');
Route::get('/form/vendedor/create', function (){
    return view('front.vendedor-create');
})->name('vendedor.create');
Route::post('/form/vendedor/store', 'web\IndexController@vendedorStore')->name('vendedor.store');
