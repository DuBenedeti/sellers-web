@extends('master')
@section('content')
<div class="mt-3">
    <a href="{{ route('vendedor.create') }}">
    <span type="button" class="btn btn-primary"
        style="--bs-btn-padding-y: .25rem; --bs-btn-padding-x: .5rem; --bs-btn-font-size: .75rem;">
    Cadastrar Vendedor
    </span>
    </a>
</div>
<table class="table table-hover mt-3" style="background-color: #efefef;">
    <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Nome</th>
      <th scope="col">Email</th>
      <th scope="col">Comissões</th>
      <th scope="col">Vendas</th>
    </tr>
  </thead>
  <tbody>
    @foreach($sellers as $seller)
    <tr>
      <td>{{$seller->id}}</td>
      <td>{{$seller->name}}</td>
      <td>{{$seller->email}}</td>
      <td>R${{ round($seller->commission, 2) ?? 0.00}}</td>
      <td>

          <a  href="{{route('vendas.index', $seller->id)}}"> <span class="btn btn-small" style="background-color: #c5c5c5;">Vendas</span> </a>

      </td>

    </tr>
    @endforeach
    
  </tbody>
</table>
@endsection