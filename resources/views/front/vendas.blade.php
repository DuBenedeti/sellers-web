@extends('master')
@section('content')
<table class="table table-hover mt-3" style="background-color: #efefef;">
    <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Nome</th>
      <th scope="col">Email</th>
      <th scope="col">Valor da Venda</th>
      <th scope="col">Comissão</th>
    </tr>
  </thead>
  <tbody>
    @foreach($sales as $sale)
    <tr>
      <td>{{$sale->id}}</td>
      <td>{{$sale->name}}</td>
      <td>{{$sale->email}}</td>
      <td>{{$sale->sale_value}}</td>
      <td>R${{$sale->commission}}</td>
      

    </tr>
    @endforeach
    
  </tbody>
</table>
@endsection