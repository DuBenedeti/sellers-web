@extends('master')
@section('content')
<form action="{{route('vendedor.store')}}" method="post">
@csrf
  <div class="mt-3">
   
    <div class="mb-3 mt-2">
      <label for="name" class="form-label">Nome</label>
      <input type="text"  class="form-control" id="name" name="name" required>
    </div>
    <div class="mb-3 mt-2">
      <label for="email" class="form-label">E-mail</label>
      <input type="email" class="form-control" id="email" name="email" required>
    </div>
  </div>
  <button class="btn btn-success" type="submit">Salvar</button>
</form>
@endsection