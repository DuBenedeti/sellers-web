@extends('master')
@section('content')
<form action="{{route('vendas.store')}}" method="post">
@csrf
  <div class="mt-3">
    <label for="sale_value" class="form-label">Selecione um Vendedor</label>
    <select class="form-select" aria-label="Default select example" name="sellers_id" required>
      <option selected disabled>Selecione um vendedor</option>
      @foreach($sellers as $seller)
      <option value="{{$seller->id}}">{{$seller->name}}</option>
      @endforeach
    </select>
    <div class="mb-3 mt-2">
      <label for="sale_value" class="form-label">Valor da venda</label>
      <input type="number" min="0" step="0.01" class="form-control" id="sale_value" name="sale_value" required>
    </div>
  </div>
  <button class="btn btn-success" type="submit">Salvar</button>
</form>
@endsection