<nav class="navbar navbar-expand-lg bg-light" style="background-color: #b2b2b2 !important">
  <div class="container-fluid">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link" href="{{route('index')}}">Vendedores</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('vendas.create')}}">Cadastrar Venda</a>
        </li>
      </ul>
    </div>
  </div>
</nav>